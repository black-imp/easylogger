#include "elog_file.h"

/**
 * EasyLogger flile log pulgin port initialize
 *
 * @return result
 */
ElogErrCode elog_file_port_init(void)
{
    ElogErrCode result = ELOG_NO_ERR;

    /* add your code here */

    return result;
}

/**
 * file log lock
 */
void elog_file_port_lock(void) {

    /* add your code here */

}

/**
 * file log unlock
 */
void elog_file_port_unlock(void) {

    /* add your code here */

}

/**
 * file log deinit
 */
void elog_file_port_deinit(void) {

    /* add your code here */

}
