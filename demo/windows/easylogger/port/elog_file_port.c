#include <elog_file.h>

/**
 * EasyLogger flile log pulgin port initialize
 *
 * @return result
 */
ElogErrCode elog_file_port_init(void) {
    ElogErrCode result = ELOG_NO_ERR;

    /* do noting, using elog_port.c's locker only */

    return result;
}

/**
 * file log lock
 */
void elog_file_port_lock(void)
{
    /* do noting, using elog_port.c's locker only */
}

/**
 * file log unlock
 */
void elog_file_port_unlock(void)
{
    /* do noting, using elog_port.c's locker only */
}
/**
 * file log deinit
 */
void elog_file_port_deinit(void)
{
    /* do noting, using elog_port.c's locker only */
}
